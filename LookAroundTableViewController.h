//
//  LookAroundTableViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 3/14/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface LookAroundTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property UITableViewController *tableViewController;
@property NSMutableArray *dealArray;
@property NSMutableArray *dealArrayAfterCalculateDistance;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property CLLocationManager *locationManager;
@property CLLocation *userLocation;
@property MKUserLocation *userLocationFromPreviousView;

- (IBAction)backToParent:(id)sender;

@end
