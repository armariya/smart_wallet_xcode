//
//  LookAroundViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 2/21/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface LookAroundViewController : UIViewController <MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)changeModal:(id)sender;
@property NSMutableArray *dealArray;
@property NSMutableDictionary *dealDictionary;
//- (void) detailButtonPressed:(UIButton *)button;
@end
