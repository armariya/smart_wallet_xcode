//
//  MapAroundViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 2/7/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface MapAroundViewController : UIViewController

@property GMSMapView *mapView;
@property(nonatomic,retain) CLLocationManager *locationManager;

@end
