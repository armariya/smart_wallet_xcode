//
//  RegisterViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMDateSelectionViewController.h"

@interface RegisterViewController : UIViewController <UIAlertViewDelegate, RMDateSelectionViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *displayNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *retypePasswrodTextField;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;

- (IBAction)submitButtonPressed:(id)sender;
- (IBAction)backgroundPressed:(id)sender;
- (IBAction)openDateSelectionViewController:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;

- (NSString *)getDateNow;
- (void)showAlert;
@end
