//
//  reverseArray.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/2/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray(calculateDistanceInArray)

- (NSMutableArray *)calculateDistanceInArray;

@end
