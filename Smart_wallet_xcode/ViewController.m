//
//  ViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getDate"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *myArray = (NSMutableArray *) responseObject;
             NSLog(@"%@", myArray);
             for (id item in myArray) {
                 NSDictionary *myDictionary = (NSDictionary *) item;
                 NSLog(@"%f", [[[myDictionary objectForKey:@"myDate"] objectForKey:@"$date"] doubleValue]);
                 double unixTimeStamp = [[[myDictionary objectForKey:@"myDate"] objectForKey:@"$date"] doubleValue] / 1000;
                 NSDate *date = [NSDate dateWithTimeIntervalSince1970:unixTimeStamp];
                 NSLog(@"%@", date);
             }

         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
