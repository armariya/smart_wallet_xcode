//
//  PromotionDetailViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromotionDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property NSDictionary *promotionDictionary;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property UITableViewController *tableViewController;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)commentButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *commentTextField;
@property NSMutableArray *commentArray;
@property (strong, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentCountLabel;
- (IBAction)likeButtonPressed:(id)sender;

- (IBAction)backBarButtonPressed:(id)sender;

@end
