//
//  PushCouponSegue.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/17/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "PushCouponSegue.h"
#import "CouponDetailViewController.h"
#import "CouponViewController.h"

@implementation PushCouponSegue

- (void)perform {
    CouponViewController *sourceViewController = (CouponViewController *) [self sourceViewController];
    CouponDetailViewController *destinationViewController = (CouponDetailViewController *) [self destinationViewController];
    
    destinationViewController.couponDictionary = sourceViewController.selectedCoupon;
    [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];
    NSLog(@"%@", sourceViewController.navigationController);
    
}

@end
