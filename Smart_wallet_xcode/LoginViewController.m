//
//  LoginViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import "LoginViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "StringToMD5.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.checkLogin = NO;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
    UIToolbar *bgToolBar = [[UIToolbar alloc] initWithFrame:self.view.frame];
    bgToolBar.barStyle = UIBarStyleDefault;
    [self.view.superview insertSubview:bgToolBar belowSubview:self.view];
}

- (void)viewWillAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"viewWIllAppear");
    NSString *status = [defaults objectForKey:@"status"];
    if ([status isEqualToString:@"login"]) {
        NSLog(@"in if");
        self.checkLogin = YES;
        [self performSegueWithIdentifier:@"loginSuccess" sender:self];
        NSLog(@"PERFORM");
    } else {
        NSLog(@"in else");
        self.checkLogin = NO;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)loginButtonPressed:(id)sender {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:@"http://localhost:8082/login"
       parameters:@{
                    @"email": self.emailTextField.text,
                    @"password": [self.passwordTextField.text convertStringToMD5],
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSMutableArray *userArray = (NSMutableArray *) responseObject;
              if ([userArray count] != 0) {
                  NSDictionary *userInformation = (NSDictionary *) [userArray objectAtIndex:0];
                  if ([self.emailTextField.text isEqualToString:[userInformation objectForKey:@"email"]] && [[self.passwordTextField.text convertStringToMD5] isEqualToString:[userInformation objectForKey:@"password"]]) {
                      self.checkLogin = YES;
                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                      [defaults setObject:self.emailTextField.text forKey:@"username"];
                      [defaults setObject:self.passwordTextField.text forKey:@"password"];
                      [defaults setObject:[userInformation objectForKey:@"displayName"] forKey:@"displayName"];
                      [defaults setObject:@"login" forKey:@"status"];
                      [self performSegueWithIdentifier:@"loginSuccess" sender:self];
                  }
              }
              else {
                  [self showAlert];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
          }];
}


- (IBAction)backgroundPressed:(id)sender {
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email or password are wrong."
                                                        message:@"Please try again"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

@end
