//
//  AppDelegate.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property CLLocationManager *locationManager;
@property CLLocation *userLocation;
@property NSMutableArray *dealArray;
- (void) startStandardUpdates;
- (void) registerRegionWithCircularOverlay:(MKCircle*)overlay andIdentifier:(NSString*)identifier;
- (void) removeAllRegion:(NSTimer *) timer;
@end
