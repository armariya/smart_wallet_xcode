//
//  MapPoint.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 2/21/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapPoint : NSObject <MKAnnotation>
@property CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSNumber *tag;
@property (strong, nonatomic) NSString *subTitle;

@end
