//
//  RegisterViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import "RegisterViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "StringToMD5.h"
#import <RMDateSelectionViewController/RMDateSelectionViewController.h>

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIToolbar *bgToolBar = [[UIToolbar alloc] initWithFrame:self.view.frame];
    bgToolBar.barStyle = UIBarStyleDefault;
    [self.view.superview insertSubview:bgToolBar belowSubview:self.view];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//RMDateSelectorViewController
- (void)dateSelectionViewController:(RMDateSelectionViewController *)vc didSelectDate:(NSDate *)aDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [dateFormatter stringFromDate:aDate];
    [self.dateButton setTitle:dateString forState:UIControlStateNormal];
}

- (void)dateSelectionViewControllerDidCancel:(RMDateSelectionViewController *)vc {
    
}

- (IBAction)openDateSelectionViewController:(id)sender {
    [self.emailTextField resignFirstResponder];
    [self.displayNameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.retypePasswrodTextField resignFirstResponder];
    RMDateSelectionViewController *dateSelectionVC = [RMDateSelectionViewController dateSelectionController];
    dateSelectionVC.delegate = self;
    [dateSelectionVC show];
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//End RMDateSelectorViewController

- (IBAction)submitButtonPressed:(id)sender {
    if ([self.passwordTextField.text isEqualToString:self.retypePasswrodTextField.text]) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = @{
                                     @"email": self.emailTextField.text,
                                     @"displayName": self.displayNameTextField.text,
                                     @"password": [self.passwordTextField.text convertStringToMD5],
                                     @"birthDate": self.dateButton.titleLabel.text,
                                     @"type": @"normal"
                                     };
        [manager POST:@"http://localhost:8082/insertNormalUser" parameters:parameters
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  [self.navigationController popViewControllerAnimated:YES];
              }
              failure:^(AFHTTPRequestOperation *opeartion, NSError *error) {
                  NSLog(@"Error: %@", error);
              }];
    } else {
        [self showAlert];
        [self.passwordTextField becomeFirstResponder];
    }
}

- (IBAction)backgroundPressed:(id)sender {
    [self.emailTextField resignFirstResponder];
    [self.displayNameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.retypePasswrodTextField resignFirstResponder];
}


- (NSString *)getDateNow {
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    NSLog(@"%@", dateString);
    return dateString;
}

- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Password and Retype Password mismatch"
                                                        message:@"Please try again"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

@end
