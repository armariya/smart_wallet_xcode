//
//  LoginViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property BOOL checkLogin;

- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)backgroundPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

- (void)showAlert;
@end
