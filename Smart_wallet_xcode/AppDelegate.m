//
//  AppDelegate.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@implementation AppDelegate

//location phase
- (void) startStandardUpdates {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 100;
    [self.locationManager setPausesLocationUpdatesAutomatically:YES];
    [self.locationManager setActivityType:CLActivityTypeAutomotiveNavigation];
    
    
    [self.locationManager startUpdatingLocation];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    //get user location now
    self.userLocation = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    NSLog(@"did update location: %f, %f", location.coordinate.latitude, location.coordinate.longitude);

    AFHTTPRequestOperationManager *myManager = [AFHTTPRequestOperationManager manager];
    [myManager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *dealArray = (NSMutableArray *) responseObject;
             NSMutableArray *dealArrayAfterCalculateDistance = [[NSMutableArray alloc] init];
             for (id item in dealArray) {
                 NSDictionary *dictionary = (NSDictionary *)item;
                 NSMutableDictionary *mutableDictionary = [dictionary mutableCopy];
                 NSDictionary *location = [dictionary objectForKey:@"location"];
                 CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:[[location objectForKey:@"latitude"] doubleValue]longitude:[[location objectForKey:@"longitude"] doubleValue]];
                 CLLocationDistance distanceFromUserLocation = [self.userLocation distanceFromLocation:destinationLocation]; //meters
                 NSNumber *distance = [NSNumber numberWithDouble:distanceFromUserLocation];
                 [mutableDictionary setObject:distance forKey:@"distance"];
                 [dealArrayAfterCalculateDistance addObject:mutableDictionary];
                 
                 NSSortDescriptor *sortDescriptor;
                 sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                              ascending:YES];
                 NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                 NSArray *sortedArray;
                 sortedArray = [dealArrayAfterCalculateDistance sortedArrayUsingDescriptors:sortDescriptors];
                 
                 dealArrayAfterCalculateDistance = [sortedArray mutableCopy];
             }
             
             self.dealArray = dealArrayAfterCalculateDistance;
             
             int count = 1;
             
             for (id item in dealArrayAfterCalculateDistance) {
                 if (count <= 20) {
                     NSDictionary *dictionary = (NSDictionary *) item;
                     NSDictionary *locationDictionary = [dictionary objectForKey:@"location"];
                     //CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:[[locationDictionary objectForKey:@"latitude"] doubleValue] longitude:[[locationDictionary objectForKey:@"longitude"] doubleValue]];
                     //CLLocationDistance distance  = [self.userLocation distanceFromLocation:destinationLocation];
                     double distance = [[dictionary objectForKey:@"distance"] doubleValue];
                     if (distance <= 1000.0) {
                         NSNumber *latitude = [NSNumber numberWithDouble:[[locationDictionary objectForKey:@"latitude"] doubleValue]];
                         NSNumber *longitude = [NSNumber numberWithDouble:[[locationDictionary objectForKey:@"longitude"] doubleValue]];
                         CLLocationCoordinate2D testLocation =  CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
                         MKCircle *testCircle = [MKCircle circleWithCenterCoordinate:testLocation radius:350];
                         [self registerRegionWithCircularOverlay:testCircle andIdentifier:[[dictionary objectForKey:@"_id"] objectForKey:@"$oid"]];
                     }
                     count = count + 1;
                 }
                 else {
                     break;
                 }
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

- (void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"ENTER REGION: %@", [region identifier]);
    int count = 1;
    for (id item in self.dealArray) {
        if (count <= 20) {
            NSDictionary *dictionary = (NSDictionary *) item;
            if ([[region identifier] isEqualToString:[[dictionary objectForKey:@"_id"] objectForKey:@"$oid"]]) {
                NSLog(@"notification");
                NSDate *alertDate = [[NSDate date]
                                     dateByAddingTimeInterval:3
                                     ];
                UIApplication *app = [UIApplication sharedApplication];
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                
                localNotification.fireDate = alertDate;
                localNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
                localNotification.repeatInterval = 0;
                localNotification.applicationIconBadgeNumber++;
                [localNotification setSoundName:UILocalNotificationDefaultSoundName];
                NSString *topic = [dictionary objectForKey:@"topic"];
                NSString *whoadded = [dictionary objectForKey:@"whoadded"];
                localNotification.alertBody = [NSString stringWithFormat:@"%@ โดย %@ อยู่ใกล้ๆนี้", topic, whoadded];
                [app scheduleLocalNotification:localNotification];
            }
        }
        else {
            break;
        }
        count = count + 1;
    }
}

- (void) locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    //do nothing
}

- (void)registerRegionWithCircularOverlay:(MKCircle*)overlay andIdentifier:(NSString*)identifier {
    CLLocationDegrees radius = overlay.radius;
    
    //reduce radius if too large
    //if (radius > self.locManager.maximumRegionMonitoringDistance) {
    //    radius = self.locManager.maximumRegionMonitoringDistance;
    //}
    //end reduce radius if too large
    
    CLCircularRegion *geoRegion = [[CLCircularRegion alloc]
                                 initWithCenter:overlay.coordinate
                                 radius:radius
                                 identifier:identifier];
    [self.locationManager startMonitoringForRegion:geoRegion];
}

- (void) removeAllRegion:(NSTimer *) timer{
    for (id region in [self.locationManager monitoredRegions]) {
        [self.locationManager stopMonitoringForRegion:region];
    }

}
//end location phase

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [NewRelicAgent startWithApplicationToken:@"AA3a176483f9da30c43aa605230280f75940b05913"];
    //[GMSServices provideAPIKey:@"AIzaSyBQlqlNoDXBfjPSKPIN61LbJ-2Lw2_yJUs"];
    
    //location phase
    [self startStandardUpdates];
    NSTimer* myTimer = [NSTimer scheduledTimerWithTimeInterval: 60*2 target: self
                                                      selector: @selector(removeAllRegion:) userInfo: nil repeats: YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *dealArray = (NSMutableArray *) responseObject;
             NSMutableArray *dealArrayAfterCalculateDistance = [[NSMutableArray alloc] init];
             for (id item in dealArray) {
                 NSDictionary *dictionary = (NSDictionary *)item;
                 NSMutableDictionary *mutableDictionary = [dictionary mutableCopy];
                 NSDictionary *location = [dictionary objectForKey:@"location"];
                 CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:[[location objectForKey:@"latitude"] doubleValue]longitude:[[location objectForKey:@"longitude"] doubleValue]];
                 CLLocationDistance distanceFromUserLocation = [self.userLocation distanceFromLocation:destinationLocation]; //meters
                 NSNumber *distance = [NSNumber numberWithDouble:distanceFromUserLocation];
                 [mutableDictionary setObject:distance forKey:@"distance"];
                 [dealArrayAfterCalculateDistance addObject:mutableDictionary];
                 
                 NSSortDescriptor *sortDescriptor;
                 sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                              ascending:YES];
                 NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                 NSArray *sortedArray;
                 sortedArray = [dealArrayAfterCalculateDistance sortedArrayUsingDescriptors:sortDescriptors];
                 
                 dealArrayAfterCalculateDistance = [sortedArray mutableCopy];
             }
             
             self.dealArray = dealArrayAfterCalculateDistance;
             
             int count = 1;
             
             for (id item in dealArrayAfterCalculateDistance) {
                 if (count <= 20) {
                     NSDictionary *dictionary = (NSDictionary *) item;
                     NSDictionary *locationDictionary = [dictionary objectForKey:@"location"];
                     //CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:[[locationDictionary objectForKey:@"latitude"] doubleValue] longitude:[[locationDictionary objectForKey:@"longitude"] doubleValue]];
                     //CLLocationDistance distance  = [self.userLocation distanceFromLocation:destinationLocation];
                     double distance = [[dictionary objectForKey:@"distance"] doubleValue];
                     if (distance <= 1000.0) {
                         NSNumber *latitude = [NSNumber numberWithDouble:[[locationDictionary objectForKey:@"latitude"] doubleValue]];
                         NSNumber *longitude = [NSNumber numberWithDouble:[[locationDictionary objectForKey:@"longitude"] doubleValue]];
                         CLLocationCoordinate2D testLocation =  CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
                         MKCircle *testCircle = [MKCircle circleWithCenterCoordinate:testLocation radius:350];
                         [self registerRegionWithCircularOverlay:testCircle andIdentifier:[[dictionary objectForKey:@"_id"] objectForKey:@"$oid"]];
                     }
                     count = count + 1;
                 }
                 else {
                     break;
                 }
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    /*NSNumber *latitude = [NSNumber numberWithDouble:13.7500];
    NSNumber *longitude = [NSNumber numberWithDouble:100.4667];
    CLLocationCoordinate2D testLocation =  CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    MKCircle *testCircle = [MKCircle circleWithCenterCoordinate:testLocation radius:300];
    [self registerRegionWithCircularOverlay:testCircle andIdentifier:@"testRegion"];*/
    //end location phase
    
    
    //local notification
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    //AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *dealsArray = (NSMutableArray *) responseObject;
             for (id dictionary in dealsArray) {
                 //NSDate *alertDate = [[NSDate date]
                    //                  dateByAddingTimeInterval:3];
                 NSDate *alertDate = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *)[dictionary objectForKey:@"end_date"] objectForKey:@"$date"] doubleValue] /1000.0];
                 UIApplication *app = [UIApplication sharedApplication];
                 UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                 
                 localNotification.fireDate = alertDate;
                 localNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
                 localNotification.repeatInterval = 0;
                 localNotification.applicationIconBadgeNumber++;
                 [localNotification setSoundName:UILocalNotificationDefaultSoundName];
                 localNotification.alertBody = [NSString stringWithFormat:@"%@ from \"%@\" will end today.", [dictionary objectForKey:@"topic"], [dictionary objectForKey:@"whoadded"]];
                 [app scheduleLocalNotification:localNotification];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *dealsArray = (NSMutableArray *) responseObject;
             for (id dictionary in dealsArray) {
                 //NSDate *alertDate = [[NSDate date]
                    //                  dateByAddingTimeInterval:3];
                 NSDate *alertDate = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *)[dictionary objectForKey:@"end_date"] objectForKey:@"$date"] doubleValue]];
                 UIApplication *app = [UIApplication sharedApplication];
                 UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                 
                 localNotification.fireDate = alertDate;
                 localNotification.timeZone = [[NSCalendar currentCalendar] timeZone];
                 localNotification.repeatInterval = 0;
                 localNotification.applicationIconBadgeNumber++;
                 [localNotification setSoundName:UILocalNotificationDefaultSoundName];
                 localNotification.alertBody = [NSString stringWithFormat:@"%@ from \"%@\" will end today.", [dictionary objectForKey:@"topic"], [dictionary objectForKey:@"whoadded"]];
                 [app scheduleLocalNotification:localNotification];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [application setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
