//
//  PushPromotionSegue.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/17/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "PushPromotionSegue.h"
#import "PromotionViewController.h"
#import "PromotionDetailViewController.h"

@implementation PushPromotionSegue

- (void) perform {
    PromotionViewController *sourceViewController = (PromotionViewController *) [self sourceViewController];
    PromotionDetailViewController *destinationViewController = (PromotionDetailViewController *) [self destinationViewController];
    
    destinationViewController.promotionDictionary = sourceViewController.selectedPromotion;
    [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];
    NSLog(@"%@", sourceViewController.navigationController);
}

@end
