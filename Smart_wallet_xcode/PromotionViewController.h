//
//  PromotionViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/14/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PromotionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property UITableViewController *tableViewController;
@property NSMutableArray *promotionMutableArray;
@property NSDictionary *selectedPromotion;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void) likeButtonPressed:(UIButton *)button;

- (void) refresh: (id)sender;

@end
