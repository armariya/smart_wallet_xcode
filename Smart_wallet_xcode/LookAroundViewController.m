//
//  LookAroundViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 2/21/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "LookAroundViewController.h"
#import "MapPoint.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "LookAroundTableViewController.h"
#import "PromotionDetailViewController.h"
#import "CouponDetailViewController.h"

@interface LookAroundViewController ()

@end

@implementation LookAroundViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    MKUserLocation *userLocation = self.mapView.userLocation;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 1000, 1000);
    [self.mapView setRegion:region animated:NO];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.dealArray = (NSMutableArray *) responseObject;
             int count = 0;
             for (id deal in self.dealArray) {
                 NSLog(@"HELLO");
                 NSDictionary *dealDictionary = (NSDictionary *) deal;
                 MapPoint *point = [[MapPoint alloc] init];
                 point.title = [NSString stringWithFormat:@"%@ - %@", [dealDictionary objectForKey:@"topic"], [dealDictionary objectForKey:@"whoadded"]];
                 point.tag = [NSNumber numberWithInt:count];
                 NSDictionary *locationDictionary = [dealDictionary objectForKey:@"location"];
                 CLLocationDegrees latitude = [[locationDictionary objectForKey:@"latitude"] doubleValue];
                 CLLocationDegrees longitude = [[locationDictionary objectForKey:@"longitude"] doubleValue];
                 NSLog(@"Eiei %f %f", latitude, longitude);
                 point.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
                 [self.mapView addAnnotation:point];
                 count = count + 1;
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    
    //demo annotation view
    /*MapPoint *point = [[MapPoint alloc] init];
    point.title = @"Hello World This is my place.";
    point.subTitle = @"Why this subtitle not shown?";
    point.coordinate = CLLocationCoordinate2DMake(13.7000, 100.4467);
    [self.mapView addAnnotation:point];*/
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"Annotation detail button pressed");
    
    MapPoint *point = (MapPoint *)view.annotation;
    self.dealDictionary = [self.dealArray objectAtIndex:[point.tag intValue]];
    if ([[self.dealDictionary objectForKey:@"kind"] isEqualToString:@"promotion"]) {
        [self performSegueWithIdentifier:@"showPromotionDetail" sender:self];
    }
    else if ([[self.dealDictionary objectForKey:@"kind"] isEqualToString:@"coupon"]) {
        [self performSegueWithIdentifier:@"showCouponDetail" sender:self];
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    mapView.centerCoordinate = userLocation.location.coordinate;
    NSLog(@"%g", userLocation.location.coordinate.latitude);
    NSLog(@"%g", userLocation.location.coordinate.longitude);
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    MKPinAnnotationView *customPinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    customPinView.animatesDrop = YES;
    customPinView.canShowCallout = YES;
    
    UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    /*[detailButton addTarget:self
                     action:@selector(detailButtonPressed:)
           forControlEvents:UIControlEventTouchDown];
    */
    customPinView.rightCalloutAccessoryView = detailButton;
    
    return customPinView;
}

/*- (void) detailButtonPressed:(UIButton *)button {
    
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"sendUserLocation"]) {
        LookAroundTableViewController *lookAroundTableViewController = (LookAroundTableViewController *) segue.destinationViewController;
        lookAroundTableViewController.userLocationFromPreviousView = self.mapView.userLocation;
    }
    else if ([[segue identifier] isEqualToString:@"showPromotionDetail"]) {
        PromotionDetailViewController *promotionDetailViewController = (PromotionDetailViewController *) segue.destinationViewController;
        promotionDetailViewController.promotionDictionary = self.dealDictionary;
    }
    else if ([[segue identifier] isEqualToString:@"showCouponDetail"]) {
        CouponDetailViewController *couponDetailViewController = (CouponDetailViewController *) segue.destinationViewController;
        couponDetailViewController.couponDictionary = self.dealDictionary;
    }
}

- (IBAction)changeModal:(id)sender {
    [self performSegueWithIdentifier:@"sendUserLocation" sender:self];
}
@end
