//
//  CouponDetailViewController.h
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/28/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponDetailViewController : UIViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property NSDictionary *couponDictionary;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *allAvailableCouponLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableCouponPerDay;
@property (weak, nonatomic) IBOutlet UILabel *topicLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *useCouponButton;
@property (strong, nonatomic) IBOutlet UITextField *commentLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property UITableViewController *tableViewController;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property NSMutableArray *commentArray;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (IBAction)useThisCouponButtonPressed:(id)sender;

- (IBAction)commentButtonPressed:(id)sender;

- (IBAction)backgroundPressed:(id)sender;
- (void)showAlert;
- (void)checkUseCoupon;
@end
