//
//  PromotionDetailViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 12/30/2556 BE.
//  Copyright (c) 2556 Ariya Lawanitchanon. All rights reserved.
//

#import "PromotionDetailViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "UIView+Toast.h"

@interface PromotionDetailViewController ()

@end

@implementation PromotionDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableViewController = [[UITableViewController alloc] init];
    self.tableViewController.tableView = self.tableView;
    
	// Do any additional setup after loading the view.    
    UIToolbar *bgToolBar = [[UIToolbar alloc] initWithFrame:self.view.frame];
    bgToolBar.barStyle = UIBarStyleDefault;
    [self.view.superview insertSubview:bgToolBar belowSubview:self.view];
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
}
- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar.topItem setTitle:@"Promotion"];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
    //[self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    //[self.navigationController.navigationBar setBarTintColor:[UIColor clearColor]];
    
    NSString *fileName = [self.promotionDictionary objectForKey:@"logoURL"];
    if ([fileName isEqualToString:@""]) {
        fileName = @"noImage_logo.png";
    }
    //NSString *urlString = [NSString stringWithFormat:@"http://webserv.kmitl.ac.th/armariya/%@", fileName];
    NSString *urlString = [NSString stringWithFormat:@"http://localhost/salemate_webapp/logos/%@", fileName];
    UIImage *logoImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
    self.logoImageView.image = logoImage;
    
    [self.topicLabel setText:[self.promotionDictionary objectForKey:@"topic"]];
    [self.descriptionLabel setText:[self.promotionDictionary objectForKey:@"description"]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    UIImage *heart_empty = [UIImage imageNamed:@"heart.png"];
    UIImage *heart_fill = [UIImage imageNamed:@"heart2.png"];
    
    NSMutableArray *likeArray = [self.promotionDictionary objectForKey:@"likes"];
    for (id name in likeArray) {
        if ([name isEqualToString:username]) {
            [self.likeButton setImage:heart_fill forState:UIControlStateNormal];
            break;
        }
        else {
            [self.likeButton setImage:heart_empty forState:UIControlStateNormal];
        }
    }
    
    [self.likeCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"likes"] count]]];
    [self.commentCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"comments"] count]]];

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    
    NSDate *start_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [self.promotionDictionary objectForKey:@"start_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *start_date_string = [dateFormatter stringFromDate:start_date];
    
    NSDate *end_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [self.promotionDictionary objectForKey:@"end_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *end_date_string = [dateFormatter stringFromDate:end_date];
    
    //NSString *dateString = [NSString stringWithFormat:@"%@ - %@", [self.promotionDictionary objectForKey:@"start_date"], [self.promotionDictionary objectForKey:@"end_date"]];
    NSString *dateString = [NSString stringWithFormat:@"%@ - %@", start_date_string, end_date_string];
    [self.dateLabel setText:dateString];
    
    self.commentArray = [self.promotionDictionary objectForKey:@"comments"];
}

- (void)viewDidAppear:(BOOL)animated {
    //[self.topicLabel sizeToFit];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)likeButtonPressed:(id)sender {
    UIImage *heart_empty = [UIImage imageNamed:@"heart.png"];
    UIImage *heart_fill = [UIImage imageNamed:@"heart2.png"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    
    if ([self.likeButton currentImage] == heart_empty) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:@"http://localhost:8082/likeOnePromotion"
          parameters:@{@"id": [[self.promotionDictionary objectForKey:@"_id"] objectForKey:@"$oid"],
                       @"username": username
                       }
             success:^(AFHTTPRequestOperation *operation, id responseObject){
                 NSLog(@"like");
                 [self.likeButton setImage:heart_fill forState:UIControlStateNormal];
                 NSMutableArray *promotionMutableArray = (NSMutableArray *) responseObject;
                 self.promotionDictionary = (NSDictionary *)[promotionMutableArray objectAtIndex:0];
                 [self.likeCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"likes"] count]]];
                 [self.commentCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"comments"] count]]];
                 [self.tableViewController.tableView reloadData];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", [error debugDescription]);
                 NSLog(@"Error: %@", [error localizedDescription]);
             }];
    }
    else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:@"http://localhost:8082/dislikeOnePromotion"
          parameters:@{@"id": [[self.promotionDictionary objectForKey:@"_id"] objectForKey:@"$oid"],
                       @"username": username}
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"dislike");
                 [self.likeButton setImage:heart_empty forState:UIControlStateNormal];
                 NSMutableArray *promotionMutableArray = (NSMutableArray *) responseObject;
                 self.promotionDictionary = (NSDictionary *) [promotionMutableArray objectAtIndex:0];
                 [self.likeCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"likes"] count]]];
                 [self.commentCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"comments"] count]]];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }];
        [self.likeButton setImage:heart_empty forState:UIControlStateNormal];
    }
}

- (IBAction)backBarButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)commentButtonPressed:(id)sender {
    //when user click send
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *displayName = [defaults objectForKey:@"displayName"];
    NSString *commentMessage = self.commentTextField.text;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:@"http://localhost:8082/insertCommentToDeal"
       parameters:@{
                    @"id": [[self.promotionDictionary objectForKey:@"_id"] objectForKey:@"$oid"],
                    @"displayName": displayName,
                    @"commentMessage": commentMessage
                    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              NSLog(@"Success insert comment");
              self.commentTextField.text = @"";
              NSMutableArray *temporaryMutableArray = (NSMutableArray *) responseObject;
              NSDictionary *temporaryDictionary =  [temporaryMutableArray objectAtIndex:0];
              self.promotionDictionary = temporaryDictionary;
              self.commentArray = [self.promotionDictionary objectForKey:@"comments"];
              [self.tableView reloadData];
              [self.view makeToast:@"Comment success" duration:2.0 position:@"center"];
              [self.commentCountLabel setText:[NSString stringWithFormat:@"%lu", (unsigned long)[[self.promotionDictionary objectForKey:@"comments"] count]]];
              [self.commentTextField resignFirstResponder];
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
          }
     ];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.commentArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSUInteger row = [indexPath row];
    
    UILabel *displayNameLabel = (UILabel *) [cell.contentView viewWithTag:1];
    [displayNameLabel setText:[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"userDisplayname"]];
    
    UILabel *commentMessageLabel = (UILabel *) [cell.contentView viewWithTag:2];
    [commentMessageLabel setText:[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"commentMessage"]];
    [commentMessageLabel sizeToFit];
    
    UILabel *commentDateLabel = (UILabel *) [cell.contentView viewWithTag:3];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    NSDate *commentDate = [NSDate dateWithTimeIntervalSince1970:[[[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"datetime"] objectForKey:@"$date"]doubleValue] / 1000.0];
    NSString *commentDate_string = [dateFormatter stringFromDate:commentDate];
    
    [commentDateLabel setText:commentDate_string];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // setup the CATransform3D structure
    CATransform3D translation;
    translation = CATransform3DMakeTranslation(-30.0, 0.7, 0.4);
    
    // define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    if (cell.layer.position.x != 0) {
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105;
}
@end
