//
//  PushLoginSegue.m
//  SmartWalletTogether
//
//  Created by Ariya on 10/10/56 BE.
//  Copyright (c) 2556 itkmitl. All rights reserved.
//

#import "PushLoginSegue.h"
#import "LoginViewController.h"

@implementation PushLoginSegue

- (void) perform {
    LoginViewController *sourceViewController = (LoginViewController *) [self sourceViewController];
    UITabBarController *destinationViewController = (UITabBarController *) [self destinationViewController];
    
    if (sourceViewController.checkLogin == YES) {
        //[sourceViewController.navigationController presentViewController:destinationViewController animated:YES completion:nil];
        [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];
    }
}

@end
