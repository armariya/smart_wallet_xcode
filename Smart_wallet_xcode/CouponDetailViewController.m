//
//  CouponDetailViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/28/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "CouponDetailViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "UIView+Toast.h"

@interface CouponDetailViewController ()

@end

@implementation CouponDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.tableViewController = [[UITableViewController alloc] init];
    self.tableViewController.tableView = self.tableView;
}

- (void)viewDidLayoutSubviews {
    self.mainScrollView.contentSize = CGSizeMake(320, 1000);
}

- (void)viewDidAppear:(BOOL)animated {
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%@", self.couponDictionary);
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.navigationController.navigationBar.topItem setTitle:@"Coupon"];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:53.0/255.0 blue:66.0/255.0 alpha:0.0]];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setOpaque:YES];
    [self.navigationController.navigationBar setTranslucent: YES];
    
    self.allAvailableCouponLabel.text = [NSString stringWithFormat:@"%@", [self.couponDictionary objectForKey:@"allAvailableCoupon"]];
    self.availableCouponPerDay.text = [NSString stringWithFormat:@"%d", [[self.couponDictionary objectForKey:@"availableCouponPerDay"] intValue] - [[self.couponDictionary objectForKey:@"useThisDay"] intValue]];
    self.topicLabel.text = [self.couponDictionary objectForKey:@"topic"];
    
    NSString *fileName = [self.couponDictionary objectForKey:@"logoURL"];
    if ([fileName isEqualToString:@""]) {
        fileName = @"noImage_logo.png";
    }
    //NSString *urlString = [NSString stringWithFormat:@"http://webserv.kmitl.ac.th/armariya/%@", fileName];
    NSString *urlString = [NSString stringWithFormat:@"http://localhost/salemate_webapp/logos/%@", fileName];
    UIImage *logoImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
    self.logoImageView.image = logoImage;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    
    NSDate *start_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [self.couponDictionary objectForKey:@"start_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *start_date_string = [dateFormatter stringFromDate:start_date];
    
    NSDate *end_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [self.couponDictionary objectForKey:@"end_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *end_date_string = [dateFormatter stringFromDate:end_date];
    
    //NSString *dateString = [NSString stringWithFormat:@"%@ - %@", [self.promotionDictionary objectForKey:@"start_date"], [self.promotionDictionary objectForKey:@"end_date"]];
    NSString *dateString = [NSString stringWithFormat:@"%@ - %@", start_date_string, end_date_string];
    
    [self.dateLabel setText:dateString];
    self.descriptionLabel.text = [self.couponDictionary objectForKey:@"description"];
    
    if ([self.availableCouponPerDay.text integerValue] == 0) {
        [self.useCouponButton setEnabled:NO];
    } else {
        [self.useCouponButton setEnabled:YES];
    }
    
    //configuration button
    [self.useCouponButton setTitleColor:[UIColor whiteColor] forState:UIControlStateApplication];
    [self.useCouponButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    //end configuration button
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    NSMutableArray *whoUseCouponArray = (NSMutableArray *) [self.couponDictionary objectForKey:@"whoUseCouponThisDay"];
    for (id item in whoUseCouponArray) {
        if ([item isEqualToString:username]) {
            [self.useCouponButton setEnabled:NO];
        }
    }
    
    self.commentArray = [self.couponDictionary objectForKey:@"comments"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)useThisCouponButtonPressed:(id)sender {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/useCoupon"
      parameters:@{
                   @"id": [[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"]
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *tempMutableArray = (NSMutableArray *) responseObject;
             NSDictionary *tempDictionary = (NSDictionary *) [tempMutableArray objectAtIndex:0];
             self.couponDictionary = tempDictionary;
             [self showAlert];
             self.allAvailableCouponLabel.text = [NSString stringWithFormat:@"%@", [self.couponDictionary objectForKey:@"allAvailableCoupon"]];
             self.availableCouponPerDay.text = [NSString stringWithFormat:@"%d", [[self.couponDictionary objectForKey:@"availableCouponPerDay"] intValue] - [[self.couponDictionary objectForKey:@"useThisDay"] intValue]];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"ERROR: %@", error);
         }];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    //add username to array "WhoUseCouponThisDay"
    [manager GET:@"http://localhost:8082/whoUseCouponThisDay"
      parameters:@{
                   @"id": [[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"],
                   @"username": username
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"useThisCouponButtonPressed success");
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error in useThisCouponButtonPressed: %@", error);
         }];
    
    //update the coupon
    [manager GET:@"http://localhost:8082/getOneCoupon"
      parameters:@{
                   @"id": [[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"]
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSMutableArray *temporaryArray = (NSMutableArray *) responseObject;
             self.couponDictionary = [temporaryArray objectAtIndex:0];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
    
    [self.useCouponButton setEnabled:NO];
}

- (IBAction)commentButtonPressed:(id)sender {
    //when user click send
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *displayName = [defaults objectForKey:@"displayName"];
    NSString *commentMessage = self.commentLabel.text;
    NSLog(@"_id: %@ -- displayName: %@ -- comment message: %@",  [[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"], displayName, commentMessage);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:@"http://localhost:8082/insertCommentToDeal"
      parameters:@{
                   @"id": [[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"],
                   @"displayName": displayName,
                   @"commentMessage": commentMessage
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"Success insert comment");
             self.commentLabel.text = @"";
             NSMutableArray *temporaryMutableArray = (NSMutableArray *) responseObject;
             NSDictionary *temporaryDictionary =  [temporaryMutableArray objectAtIndex:0];
             self.couponDictionary = temporaryDictionary;
             self.commentArray = [self.couponDictionary objectForKey:@"comments"];
             [self.tableView reloadData];
             [self.view makeToast:@"Comment success" duration:2.0 position:@"center"];
             [self.commentLabel resignFirstResponder];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }
     ];
}

- (IBAction)backgroundPressed:(id)sender {
    [self.commentLabel resignFirstResponder];
}

- (void)showAlert {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Don't press OK if the employee didn't see this code."
                                                        message:[[self.couponDictionary objectForKey:@"_id"] objectForKey:@"$oid"]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [alertView show];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.commentArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSUInteger row = [indexPath row];
    
    UILabel *displayNameLabel = (UILabel *) [cell.contentView viewWithTag:1];
    [displayNameLabel setText:[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"userDisplayname"]];
    
    UILabel *commentMessageLabel = (UILabel *) [cell.contentView viewWithTag:2];
    [commentMessageLabel setText:[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"commentMessage"]];
    [commentMessageLabel sizeToFit];
    
    UILabel *commentDateLabel = (UILabel *) [cell.contentView viewWithTag:3];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    NSDate *commentDate = [NSDate dateWithTimeIntervalSince1970:[[[(NSDictionary *)[self.commentArray objectAtIndex:row] objectForKey:@"datetime"] objectForKey:@"$date"]doubleValue] / 1000.0];
    NSString *commentDate_string = [dateFormatter stringFromDate:commentDate];
    
    [commentDateLabel setText:commentDate_string];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // setup the CATransform3D structure
    CATransform3D translation;
    translation = CATransform3DMakeTranslation(-30.0, 0.7, 0.4);
    
    // define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    if (cell.layer.position.x != 0) {
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // do it
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel *commentMessageLabel = (UILabel *)[cell.contentView viewWithTag:2];
    
    [commentMessageLabel sizeToFit];
    return 105;
    //return commentMessageLabel.frame.size.height + 30 * 1.7;
}

- (void)checkUseCoupon {
    //do it
}
@end
