//
//  CouponViewController.h
//  
//
//  Created by Ariya Lawanitchanon on 1/25/2557 BE.
//
//

#import <UIKit/UIKit.h>

@interface CouponViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property UITableViewController *tableViewController;
@property NSMutableArray *couponMutableArray;
@property NSDictionary *selectedCoupon;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (void) likeButtonPressed:(UIButton *)button;

- (void) refresh: (id)sender;
@end
