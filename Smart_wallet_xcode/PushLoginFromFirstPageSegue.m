//
//  PushLoginFromFirstPageSegue.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 3/15/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "PushLoginFromFirstPageSegue.h"
#import "FirstPageViewController.h"

@implementation PushLoginFromFirstPageSegue

- (void) perform {
    FirstPageViewController *sourceViewController = (FirstPageViewController *) [self sourceViewController];
    UITabBarController *destinationViewController = (UITabBarController *) [self destinationViewController];
    
    if (sourceViewController.checkLogin == YES) {
        //[sourceViewController.navigationController presentViewController:destinationViewController animated:YES completion:nil];
        [sourceViewController.navigationController pushViewController:destinationViewController animated:YES];
    }
}

@end
