//
//  PromotionViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/14/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "PromotionViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "ReverseArray.h"
@interface PromotionViewController ()

@end

@implementation PromotionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    // setting view
    UIToolbar *bgToolBar = [[UIToolbar alloc] initWithFrame:self.view.frame];
    bgToolBar.barStyle = UIBarStyleDefault;
    [self.view.superview insertSubview:bgToolBar belowSubview:self.view];
    
    
    self.tableViewController = [[UITableViewController alloc] init];
    self.tableViewController.tableView = self.tableView;
    //self.tableView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.15];
    
    //setting up Refresh Control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    //Configure Refresh Control
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    //add to tableViewController
    self.tableViewController.refreshControl = refreshControl;
}

- (void) viewDidAppear:(BOOL)animated {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllPromotion"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.promotionMutableArray = (NSMutableArray *) responseObject;
             self.promotionMutableArray = [self.promotionMutableArray reverseArray];
             [self.tableViewController.tableView reloadData];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }
     ];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableViewController
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.promotionMutableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSUInteger row = [indexPath row];
    
    NSDictionary *promotionDictionary = (NSDictionary *) [self.promotionMutableArray objectAtIndex:row];
    //Configure the cell...
    UIImageView *logoImageView = (UIImageView *) [cell.contentView viewWithTag:1];
    NSString *fileName = [promotionDictionary objectForKey:@"logoURL"];
    if ([fileName isEqualToString:@""]) {
        fileName = @"noImage_logo.png";
    }
    //NSString *urlString = [NSString stringWithFormat:@"http://webserv.kmitl.ac.th/armariya/%@", fileName];
    NSString *urlString = [NSString stringWithFormat:@"http://localhost/salemate_webapp/logos/%@", fileName];
    UIImage *logoImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
    logoImageView.image = logoImage;

    
    UILabel *topicLabel = (UILabel *) [cell.contentView viewWithTag:2];
    //[topicLabel setTextColor:[UIColor whiteColor]];
    [topicLabel setText:[promotionDictionary objectForKey:@"topic"]];

    UIImage *heart_empty = [UIImage imageNamed:@"heart.png"];
    UIImage *heart_fill = [UIImage imageNamed:@"heart2.png"];
    
    UIButton *likeButton = (UIButton *) [cell.contentView viewWithTag:3];
    [likeButton setImage:heart_empty forState:UIControlStateNormal];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    for (NSString *name in [promotionDictionary objectForKey:@"likes"]) {
        if ([name isEqualToString:username]) {
            [likeButton setImage:heart_fill forState:UIControlStateNormal];
        } else {
            [likeButton setImage:heart_empty forState:UIControlStateNormal];
        }
    }
    
    [likeButton addTarget:self
                   action:@selector(likeButtonPressed:)
         forControlEvents:UIControlEventTouchDown];
    
    UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:4];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSDate *start_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [promotionDictionary objectForKey:@"start_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *start_date_string = [dateFormatter stringFromDate:start_date];
    
    NSDate *end_date = [NSDate dateWithTimeIntervalSince1970:[[(NSDictionary *) [promotionDictionary objectForKey:@"end_date"] objectForKey:@"$date"] doubleValue] / 1000.0];
    NSString *end_date_string = [dateFormatter stringFromDate:end_date];
    
    [dateLabel setText:[NSString stringWithFormat:@"%@ - %@", start_date_string, end_date_string]];
    
    UILabel *locationLabel = (UILabel *)[cell.contentView viewWithTag:5];
    [locationLabel setText:[promotionDictionary objectForKey:@"place"]];
    
    UILabel *numberOfLike = (UILabel *)[cell.contentView viewWithTag:6];
    NSMutableArray *likeArray = (NSMutableArray *) [promotionDictionary objectForKey:@"likes"];
    [numberOfLike setText: [NSString stringWithFormat:@"%lu", (unsigned long) [likeArray count]]];
    
    UILabel *numberOfComment = (UILabel *)[cell.contentView viewWithTag:7];
    NSMutableArray *commentArray = (NSMutableArray *) [promotionDictionary objectForKey:@"comments"];
    [numberOfComment setText: [NSString stringWithFormat:@"%lu", (unsigned long) [commentArray count]] ];
    
    //end Configure the cell...
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 123;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    self.selectedPromotion = [self.promotionMutableArray objectAtIndex:row];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/plusViewed"
      parameters:@{
                   @"id": [[self.selectedPromotion objectForKey:@"_id"] objectForKey:@"$oid"],
                   @"viewed": [self.selectedPromotion objectForKey:@"viewed"]
                   }
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"Plus Viewed Success");
             [self performSegueWithIdentifier:@"showPromotionDetail" sender:self];
             NSLog(@"PERFORMMM!!");
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error){
             NSLog(@"Error: %@", error);
         }];

}

/*- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //cell.backgroundColor = [UIColor clearColor];
    
    // setup the CATransform3D structure
    CATransform3D translation;
    translation = CATransform3DMakeTranslation(-30.0, 0.7, 0.4);
    
    // define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = translation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    if (cell.layer.position.x != 0) {
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"translation" context:NULL];
    [UIView setAnimationDuration:0.3];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}*/

//end tableViewController

- (void) likeButtonPressed:(UIButton *)button {
    NSLog(@"%@", [button currentImage]);
    UITableViewCell *cell = (UITableViewCell *) button.superview.superview.superview.superview;
    UITableView *tableView = (UITableView *) cell.superview.superview;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    NSUInteger row = [indexPath row];
    NSLog(@"row: %lu", (unsigned long)row);
    UIImage *heart_empty = [UIImage imageNamed:@"heart.png"];
    UIImage *heart_fill = [UIImage imageNamed:@"heart2.png"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults objectForKey:@"username"];
    
    
    if ([button currentImage] == heart_empty) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:@"http://localhost:8082/likePromotion"
          parameters:@{@"id": [[[self.promotionMutableArray objectAtIndex:row] objectForKey:@"_id"] objectForKey:@"$oid"],
                       @"username": username
                       }
             success:^(AFHTTPRequestOperation *operation, id responseObject){
                 NSLog(@"like");
                 [button setImage:heart_fill forState:UIControlStateNormal];
                 self.promotionMutableArray = (NSMutableArray *) responseObject;
                 self.promotionMutableArray = [self.promotionMutableArray reverseArray];
                 [self.tableViewController.tableView reloadData];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", [error debugDescription]);
                 NSLog(@"Error: %@", [error localizedDescription]);
             }];
    }
    else {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:@"http://localhost:8082/dislikePromotion"
          parameters:@{@"id": [[[self.promotionMutableArray objectAtIndex:row] objectForKey:@"_id"] objectForKey:@"$oid"],
                       @"username": username}
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSLog(@"dislike");
                 [button setImage:heart_empty forState:UIControlStateNormal];
                 self.promotionMutableArray = (NSMutableArray *) responseObject;
                 self.promotionMutableArray = [self.promotionMutableArray reverseArray];
                 [self.tableViewController.tableView reloadData];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }];
        [button setImage:heart_empty forState:UIControlStateNormal];
    }
}

- (void) refresh: (id)sender {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllPromotion"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.promotionMutableArray = (NSMutableArray *) responseObject;
             self.promotionMutableArray = [self.promotionMutableArray reverseArray];
             [self.tableViewController.tableView reloadData];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }
     ];
    [(UIRefreshControl *)sender endRefreshing];
}

@end
