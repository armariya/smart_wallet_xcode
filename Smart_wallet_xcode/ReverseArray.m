//
//  reverseArray.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 1/2/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "reverseArray.h"

@implementation NSMutableArray(reverseArray)

-(NSMutableArray *)reverseArray {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self count]];
    NSEnumerator *enumerator = [self reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}

@end
