//
//  StringToMD5.h
//  SmartWalletTogether
//
//  Created by Ariya on 10/8/56 BE.
//  Copyright (c) 2556 itkmitl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(convertStringToMD5)

- (NSString *) convertStringToMD5;

@end
