//
//  LookAroundTableViewController.m
//  Smart_wallet_xcode
//
//  Created by Ariya Lawanitchanon on 3/14/2557 BE.
//  Copyright (c) 2557 Ariya Lawanitchanon. All rights reserved.
//

#import "LookAroundTableViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface LookAroundTableViewController ()

@end

@implementation LookAroundTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%f %f", self.userLocationFromPreviousView.coordinate.latitude, self.userLocationFromPreviousView.coordinate.longitude);
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
    self.tableViewController = [[UITableViewController alloc] init];
    self.tableViewController.tableView = self.tableView;
    self.tableView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.15];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.userLocation = [locations lastObject];
    //NSLog(@"%f %f", self.userLocation.coordinate.latitude, self.userLocation.coordinate.longitude);
}

- (void) viewDidAppear:(BOOL)animated {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://localhost:8082/getAllDeal"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.dealArray = (NSMutableArray *) responseObject;
             self.dealArrayAfterCalculateDistance = [[NSMutableArray alloc] initWithCapacity:[self.dealArray count]];
             CLLocation *myLocation = [[CLLocation alloc] initWithLatitude:self.userLocationFromPreviousView.coordinate.latitude longitude:self.userLocationFromPreviousView.coordinate.longitude];
             for (id item in self.dealArray) {
                 NSDictionary *dictionary = (NSDictionary *)item;
                 NSMutableDictionary *mutableDictionary = [dictionary mutableCopy];
                 NSDictionary *location = [dictionary objectForKey:@"location"];
                 CLLocation *destinationLocation = [[CLLocation alloc] initWithLatitude:[[location objectForKey:@"latitude"] doubleValue]longitude:[[location objectForKey:@"longitude"] doubleValue]];
                 CLLocationDistance distanceFromUserLocation = [myLocation distanceFromLocation:destinationLocation]; //meters
                 NSNumber *distance = [NSNumber numberWithDouble:distanceFromUserLocation];
                 [mutableDictionary setObject:distance forKey:@"distance"];
                 [self.dealArrayAfterCalculateDistance addObject:mutableDictionary];
                 
                 NSSortDescriptor *sortDescriptor;
                 sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                              ascending:YES];
                 NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
                 NSArray *sortedArray;
                 sortedArray = [self.dealArrayAfterCalculateDistance sortedArrayUsingDescriptors:sortDescriptors];
                 
                 self.dealArrayAfterCalculateDistance = [sortedArray mutableCopy];
             }
             
             NSLog(@"%@", self.dealArrayAfterCalculateDistance);

             [self.tableViewController.tableView reloadData];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }
     ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dealArrayAfterCalculateDistance count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSUInteger row = [indexPath row];
    
    NSDictionary *dealDictionary = [self.dealArrayAfterCalculateDistance objectAtIndex:row];
    
    UIImageView *logoImageView = (UIImageView *) [cell.contentView viewWithTag:1];
    NSString *fileName = [dealDictionary objectForKey:@"logoURL"];
    if ([fileName isEqualToString:@""]) {
        fileName = @"noImage_logo.png";
    }
    NSString *urlString = [NSString stringWithFormat:@"http://webserv.kmitl.ac.th/armariya/%@", fileName];
    UIImage *logoImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]];
    logoImageView.image = logoImage;
    
    UILabel *topicLabel = (UILabel *) [cell.contentView viewWithTag:2];
    [topicLabel setText:[dealDictionary objectForKey:@"topic"]];

    UILabel *distanceLabel = (UILabel *) [cell.contentView viewWithTag:3];
    [distanceLabel setText:[NSString stringWithFormat:@"%.3f meters", [[dealDictionary objectForKey:@"distance"] doubleValue]]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 123;
}

- (IBAction)backToParent:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
