# coding: utf-8
from bson import Binary, Code
from bson.json_util import dumps, loads
import bottle
import pymongo
import datetime
from bson.objectid import ObjectId
from threading import Timer


#connect to mongodb in anywhere
connection = pymongo.MongoClient('localhost', 27017)
db = connection.smart_wallet

def runAllMethod():
	isDealAvailable()
	clearWhoUseCoupon()
	dailyRefillNumberUseCoupon()

def clearWhoUseCoupon():
	interval = 5 * 60
	Timer(interval, clearWhoUseCoupon).start()
	db.deals.update({'kind': 'coupon'}, {'$set': {'whoUseCouponThisDay': []}}, multi = True)
	print "clearWhoUseCoupon run!";

def dailyRefillNumberUseCoupon():
	interval = 5 * 60
	Timer(interval, dailyRefillNumberUseCoupon).start()
	couponArray = db.deals.find({'kind': 'coupon'})
	for dictionary in couponArray:
		insideArray = db.deals.find({'_id': dictionary["_id"]})
		for insideDictionary in insideArray:
			db.deals.update({'_id': dictionary["_id"]}, {'$set': {'useThisDay': 0}})
			break
	print "dailyRefillNumberUseCoupon run!"

def isDealAvailable():
	interval = 5*60*60
	Timer(interval, isDealAvailable).start()
	dealsArray = db.deals.find()
	for dictionary in dealsArray:
		insideArray = db.deals.find({'_id': dictionary['_id']})
		for insideDictionary in insideArray:
			if (dictionary['end_date'] < datetime.datetime.utcnow()):
				db.deals.update({'_id': dictionary['_id']}, {'$set': {'availableFlag': 'false'}})

		

runAllMethod()