# coding: utf-8
from bson import Binary, Code
from bson.json_util import dumps, loads
import bottle
import pymongo
import datetime
from bson.objectid import ObjectId

#connect to mongodb in anywhere
connection = pymongo.MongoClient('localhost', 27017)
db = connection.smart_wallet
#mapping with url
@bottle.route('/login', method='POST')
#normal user login
def login():
	bottle.response.headers['Content-Type'] = 'text/json'
	email = bottle.request.forms.get('email')
	password = bottle.request.forms.get('password')
	return dumps(db.users.find({'email': email, 'password': password}))

@bottle.route('/getDate', method="GET")
def getDate():
	bottle.response.headers['Content-Type'] = 'text/json'
	return dumps(db.deals.find({'_id': ObjectId("532083b7a67a6a14589c876e")}))

@bottle.route('/insertCoupon', method="GET")
#insert coupon
def insertCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	storeName = bottle.request.query.storeName
	email = bottle.request.query.email
    #combine to one string
	insertCouponString = {
	'availableFlag': "true",
	'description' : "สินค้าลดสิบเปอเซ็นต์ทุกรายการ",
	'end_date' : datetime.datetime.strptime("20/04/14", "%d/%m/%y"),
	'kind': "coupon",
	'likes' : [],
	'comments' : [],
	'place' : "Siam Paragon",
	'start_date' : datetime.datetime.strptime("08/02/14", "%d/%m/%y"),
	'topic' : "คูปองดีจากทาง armariya ลด 10% ทุกรายการ",
	'viewed' : 0,
	'logoURL': "",
	'whoadded' : storeName,
	'whoaddedEmail': email,
	'allAvailableCoupon': 100,
	'availableCouponPerDay': 10,
	'location': {
		'latitude': 13.7500,
		'longitude': 100.4467
	},
	'whoUseCouponThisDay': [],
	'useThisDay': 0
	}
	db.deals.insert(insertCouponString)

@bottle.route('/insertCouponPost', method="POST")
#insert coupon
def insertCouponPost():
	bottle.response.headers['Content-Type'] = 'text/json'
	storeName = bottle.request.forms.get('storeName')
	email = bottle.request.forms.get('email')
	latitude = bottle.request.forms.get('latitude')
	longitude = bottle.request.forms.get('longitude')
	topic = bottle.request.forms.get('topic')
	start_date = bottle.request.forms.get('start_date')
	end_date = bottle.request.forms.get('end_date')
	description = bottle.request.forms.get('description')
	place = bottle.request.forms.get('place')
	numberOfCoupon = bottle.request.forms.get('numberOfCoupon')
	numberOfAvailableCouponPerDay = bottle.request.forms.get('numberOfAvailableCouponPerDay')
	logoURL = bottle.request.forms.get('logoURL')
    #combine to one string
	insertCouponString = {
	'availableFlag': "true",
	'description' : description,
	'end_date' : datetime.datetime.strptime(end_date, "%Y-%m-%d"),
	'kind': "coupon",
	'likes' : [],
	'comments' : [],
	'place' : place,
	'start_date' : datetime.datetime.strptime(start_date, "%Y-%m-%d"),
	'topic' : topic,
	'viewed' : 0,
	'logoURL': logoURL,
	'whoadded' : storeName,
	'whoaddedEmail': email,
	'allAvailableCoupon': int(numberOfCoupon),
	'availableCouponPerDay': int(numberOfAvailableCouponPerDay),
	'location': {
		'latitude': latitude,
		'longitude': longitude
	},
	'whoUseCouponThisDay': [],
	'useThisDay': 0
	}
	db.deals.insert(insertCouponString)

@bottle.route('/editCouponPost', method="POST")
#insert coupon
def editCouponPost():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.forms.get('id')
	storeName = bottle.request.forms.get('storeName')
	email = bottle.request.forms.get('email')
	latitude = bottle.request.forms.get('latitude')
	longitude = bottle.request.forms.get('longitude')
	topic = bottle.request.forms.get('topic')
	start_date = bottle.request.forms.get('start_date')
	end_date = bottle.request.forms.get('end_date')
	description = bottle.request.forms.get('description')
	place = bottle.request.forms.get('place')
	numberOfCoupon = bottle.request.forms.get('numberOfCoupon')
	numberOfAvailableCouponPerDay = bottle.request.forms.get('numberOfAvailableCouponPerDay');

    #combine to one string
	insertCouponString = {

	'availableFlag': "true",
	'description' : description,
	'end_date' : datetime.datetime.strptime(end_date, "%Y-%m-%d"),
	'kind': "coupon",
	'likes' : [],
	'comments' : [],
	'place' : place,
	'start_date' : datetime.datetime.strptime(start_date, "%Y-%m-%d"),
	'topic' : topic,
	'viewed' : 0,
	'logoURL': "",
	'whoadded' : storeName,
	'whoaddedEmail': email,
	'allAvailableCoupon': int(numberOfCoupon),
	'availableCouponPerDay': int(numberOfAvailableCouponPerDay),
	'location': {
		'latitude': latitude,
		'longitude': longitude
	},
	'whoUseCouponThisDay': [],
	'useThisDay': 0
	}
	db.deals.update({'_id': ObjectId(_id)}, {'$set': insertCouponString})

@bottle.route('/testInsert', method='GET')
def testInsert():
	bottle.response.headers['Content-Type'] = 'text/json'
	insertString = {
	'myDate': datetime.datetime.strptime("08/02/14", "%d/%m/%y")
	}
	db.deals.insert(insertString)

@bottle.route('/insertPromotion', method="GET")
#insert coupon
def insertPromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	#combine to one string
	storeName = bottle.request.query.storeName
	email = bottle.request.query.email
	latitude = bottle.request.query.latitude
	longitude = bottle.request.query.longitude

	insertPromotionString = {
	'availableFlag': "true",
	'description' : "เมื่อซื้อเฟรนฟรายวันนี้ ลดราคาทันที 50% จากราคาปกติ เพื่อให้ทุกท่านได้อิ่มอร่อยไปกับ เฟรนฟรายจากทาง Mcdonalds",
	'end_date' : datetime.datetime.strptime("20/04/14", "%d/%m/%y"),
	'kind': "promotion",
	'likes' : [],
	'comments' : [],
	'place' : "Siam Paragon",
	'start_date' : datetime.datetime.strptime("08/02/14", "%d/%m/%y"),
	'topic' : "โปรโมชั่นใหม่จากทำง Mcdonald's ซื้อเฟรนฟรายวันนี้ ลดทันที 50%",
	'viewed' : 0,
	'logoURL': "mcdonald_logo.png",
	'whoadded' : storeName,
	'whoaddedEmail' : email,
	'location': {
		'latitude': latitude,
		'longitude': longitude
	}
	}
	db.deals.insert(insertPromotionString)

@bottle.route('/insertPromotionPost', method="POST")
#insert coupon
def insertPromotionPost():
	bottle.response.headers['Content-Type'] = 'text/json'
	#combine to one string
	storeName = bottle.request.forms.get('storeName')
	email = bottle.request.forms.get('email')
	latitude = bottle.request.forms.get('latitude')
	longitude = bottle.request.forms.get('longitude')
	topic = bottle.request.forms.get('topic')
	start_date = bottle.request.forms.get('start_date')
	end_date = bottle.request.forms.get('end_date')
	description = bottle.request.forms.get('description')
	place = bottle.request.forms.get('place')
	logoURL = bottle.request.forms.get('logoURL')

	insertPromotionString = {
	'availableFlag': "true",
	'description' : description,
	'end_date' : datetime.datetime.strptime(end_date, "%Y-%m-%d"),
	#'end_date' : datetime.datetime.strptime(end_date, "%m/%d/%Y"),
	'kind': "promotion",
	'likes' : [],
	'comments' : [],
	'place' : place,
	'start_date' : datetime.datetime.strptime(start_date, "%Y-%m-%d"),
	#'start_date' : datetime.datetime.strptime(start_date, "%m/%d/%Y"),
	'topic' : topic,
	'viewed' : 0,
	'logoURL': logoURL,
	'whoadded' : storeName,
	'whoaddedEmail' : email,
	'location': {
		'latitude': float(latitude),
		'longitude': float(longitude)
	}
	}
	db.deals.insert(insertPromotionString)

@bottle.route('/editPromotionPost', method="POST")
#insert coupon
def editPromotionPost():
	bottle.response.headers['Content-Type'] = 'text/json'
	#combine to one string
	_id = bottle.request.forms.get('id')
	storeName = bottle.request.forms.get('storeName')
	email = bottle.request.forms.get('email')
	latitude = bottle.request.forms.get('latitude')
	longitude = bottle.request.forms.get('longitude')
	topic = bottle.request.forms.get('topic')
	start_date = bottle.request.forms.get('start_date')
	end_date = bottle.request.forms.get('end_date')
	description = bottle.request.forms.get('description')
	place = bottle.request.forms.get('place')

	insertPromotionString = {
	'availableFlag': "true",
	'description' : description,
	'end_date' : datetime.datetime.strptime(end_date, "%Y-%m-%d"),
	#'end_date' : datetime.datetime.strptime(end_date, "%m/%d/%Y"),
	'kind': "promotion",
	'likes' : [],
	'comments' : [],
	'place' : place,
	'start_date' : datetime.datetime.strptime(start_date, "%Y-%m-%d"),
	#'start_date' : datetime.datetime.strptime(start_date, "%m/%d/%Y"),
	'topic' : topic,
	'viewed' : 0,
	'logoURL': "mcdonald_logo.png",
	'whoadded' : storeName,
	'whoaddedEmail' : email,
	'location': {
		'latitude': float(latitude),
		'longitude': float(longitude)
	}
	}
	db.deals.update({'_id': ObjectId(_id)}, {"$set": insertPromotionString})

@bottle.route('/insertNormalUser', method='POST')
#normal user is mobile user
def insertNormalUser():
	bottle.response.headers['Content-Type'] = 'text/json'
	#get from mobile application
	email = bottle.request.forms.get('email')
	displayName = bottle.request.forms.get('displayName')
	password = bottle.request.forms.get('password')
	birthDate = bottle.request.forms.get('birthDate')
	userType = 'normal'
	#combine to one string
	insertUserString = {
	'email': email,
	'displayName': displayName,
	'password': password,
	'birthDate': birthDate,
	'registerDate': datetime.datetime.utcnow(),
	'type': userType
	}
	db.users.insert(insertUserString)

@bottle.route('/insertStoreUser', method="options")
def insertStoreUserOptions():
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'GET', 'POST']
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    return 200

@bottle.route('/insertStoreUser', method="POST")
#store user is web user
def insertStoreUser():
    bottle.response.headers['Content-Type'] = 'text/json'
    email = bottle.request.forms.get('email')
    password = bottle.request.forms.get('password')
    storeName = bottle.request.forms.get('storeName')
    latitude = bottle.request.forms.get('latitude')
    longitude = bottle.request.forms.get('longitude')
    address = bottle.request.forms.get('address')
    logoURL = bottle.request.forms.get('logoURL')
    userType = 'store'

    insertStoreUserString = {
                        'email': email,
                        'password': password,
                        'storeName': storeName,
                        'registerDate': datetime.datetime.utcnow(),
                        'type': userType,
                        'address': address,
                        'logoURL': logoURL,
                        'location': {
                                     'latitude': float(latitude),
                                     'longitude': float(longitude)
                                     }
                        }
    db.users.insert(insertStoreUserString)
    return "insert success"

@bottle.route('/insertCommentToDeal', method="POST")
def insertCommentToDeal():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.forms.get('id')
	displayName = bottle.request.forms.get('displayName')
	commentMessage = bottle.request.forms.get('commentMessage')
	commentDictionary = {
	'userDisplayname': displayName,
	'commentMessage': commentMessage,
	'datetime': datetime.datetime.utcnow()
	}
	db.deals.update({'_id': ObjectId(_id)}, {'$addToSet': {"comments": commentDictionary}})
	return dumps(db.deals.find({"_id": ObjectId(_id)}))

@bottle.route('/getUsers', method="options")
def getUsersOptions():
    return 200;

@bottle.route('/getUsers', method="get")
def getUsers():
    bottle.response.headers['Content-Type'] = 'text/json'
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'GET', 'POST']
    return dumps(db.users.find());

@bottle.route('/getMostViewedDeal', method="get")
#get deal with most viewed
def getMostViewedDeal():
    bottle.response.headers['Content-Type'] = 'text/json'
    return dumps(db.deals.find({'availableFlag': 'true'}).sort("viewed", pymongo.DESCENDING))

@bottle.route('/getAllDeal', method="get")
#get all deal
def getAllDeal():
	bottle.response.headers['Content-Type'] = 'text/json'
	return dumps(db.deals.find({'availableFlag': 'true'}))


@bottle.route('/getAllPromotion', method="get")
#get all promotion
def getAllPromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	return dumps(db.deals.find({'kind': 'promotion', 'availableFlag': "true"}))

@bottle.route('/getAllCoupon', method="get")
#get all coupon
def getAllCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	return dumps(db.deals.find({'kind': 'coupon', 'availableFlag': "true"}))

@bottle.route('/getDealsInOneStore', method="get")
def getPromotionInOneStore():
	bottle.response.headers['Content-Type'] = 'text/json'
	bottle.response.headers['Access-Control-Allow-Origin'] = '*'
	bottle.response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'GET', 'POST']
	bottle.response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
	email = bottle.request.query.email
	storeName = bottle.request.query.storeName
	return dumps(db.deals.find({'whoaddedEmail': email, 'whoadded': storeName}))

@bottle.route('/getOnePromotion', method="GET")
def getOnePromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	bottle.response.headers['Access-Control-Allow-Origin'] = '*'
	bottle.response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'GET', 'POST']
	bottle.response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
	_id = bottle.request.query.id
	return dumps(db.deals.find({'_id': ObjectId(_id)}))

@bottle.route('/getOneCoupon', method="get")
#get all coupon
def getOneCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	bottle.response.headers['Access-Control-Allow-Origin'] = '*'
	bottle.response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'GET', 'POST']
	bottle.response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
	_id = bottle.request.query.id
	return dumps(db.deals.find({'_id': ObjectId(_id)}))


@bottle.route('/likeCoupon', method="get")
#like the deal
def likeCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$addToSet': {"likes": username}})
	return dumps(db.deals.find({'kind': 'coupon', 'availableFlag': "true"}))

@bottle.route('/dislikeCoupon', method="get")
#dislike the deal
def dislikeCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$pull': {'likes': username}})
	return dumps(db.deals.find({'kind': 'coupon', 'availableFlag': "true"}))

@bottle.route('/likeOnePromotion', method="get")
def likeOnePromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$addToSet': {"likes": username}})
	return dumps(db.deals.find({'_id': ObjectId(_id)}))

@bottle.route('/likePromotion', method="get")
#like the deal
def likePromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$addToSet': {"likes": username}})
	return dumps(db.deals.find({'kind': 'promotion', 'availableFlag': "true"}))

@bottle.route('/dislikePromotion', method="get")
#dislike the deal
def dislikePromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$pull': {'likes': username}})
	return dumps(db.deals.find({'kind': 'promotion', 'availableFlag': "true"}))

@bottle.route('/dislikeOnePromotion', method="get")
#dislike the deal
def dislikeOnePromotion():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$pull': {'likes': username}})
	return dumps(db.deals.find({'_id': ObjectId(_id)}))


@bottle.route('/plusViewed', method="GET")
def plusViewed():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	viewed = bottle.request.query.viewed
	db.deals.update({'_id': ObjectId(_id)}, {'$set': {'viewed': int(viewed)+1}})
	return dumps(db.deals.find({'_id': ObjectId(_id)}))

@bottle.route('/useCoupon', method="GET")
def useCoupon():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	dictionary = db.deals.find({'_id': ObjectId(_id)})
	for item in dictionary:
		if (int(item['availableCouponPerDay']) - int(item['useThisDay']) > 0):
			db.deals.update({'_id': ObjectId(_id)}, {'$set': {'useThisDay': int(item['useThisDay']) + 1}})
			db.deals.update({'_id': ObjectId(_id)}, {'$set': {'allAvailableCoupon': int(item['allAvailableCoupon']) - 1}})
			break;
	test = db.deals.find({'_id': ObjectId(_id)})
	for item in test:
		print item['allAvailableCoupon'];
		print item['useThisDay']
	return dumps(db.deals.find({'_id': ObjectId(_id)}))

@bottle.route('/whoUseCouponThisDay', method="GET")
def whoUseCouponThisDay():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.query.id
	username = bottle.request.query.username
	db.deals.update({'_id': ObjectId(_id)}, {'$addToSet': {'whoUseCouponThisDay': username}})

@bottle.route('/deleteDeal', method="POST")
def deleteDeal():
	bottle.response.headers['Content-Type'] = 'text/json'
	_id = bottle.request.forms.get('id')
	db.deals.remove({'_id': ObjectId(_id)});

bottle.run(host='localhost', port=8082)
